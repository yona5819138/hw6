// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyARvvs1K2ikA6oHrSybvrIavYzIto3e9mY",
    authDomain: "pbreg2-34cd3.firebaseapp.com",
    databaseURL: "https://pbreg2-34cd3.firebaseio.com",
    projectId: "pbreg2-34cd3",
    storageBucket: "pbreg2-34cd3.appspot.com",
    messagingSenderId: "171337928545"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
