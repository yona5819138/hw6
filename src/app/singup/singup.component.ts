import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
@Component({
  selector: 'singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
  
  name: string;
  password: string;
  email:string;
  code = '';
  message = '';
  required = '';
  require = true;

   specialChar = ['!','@','#','$','%','^','&','*',
                  '(',')','_','-','+','=',"'",'"',
                  ',','/','?','.','>','<','{','}',
                  '[',']','|','~','`',':',';']
  valid = false;
  validation = "";
  hide=true;
  /*getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }*/
  tosign(){
    
    this.valid = false;
    this.require = true;

    if (this.name == null || this.password == null || this.email == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  if(this.require)
    {
      for (let char of this.specialChar)
      {
        if (this.password.includes(char))
        {
          this.valid = true;
        }
      }
    }
     if (this.valid && this.require)
    {
      this.authService.signUp(this.email,this.password)
      .then(value => { 
                      this.authService.updateProfile(value.user,this.name);
                      this.authService.addUser(value.user,this.name,this.email);
                      }).then(value =>{
                                      this.router.navigate(['/welcome']);
                                      }).catch(err => { 
                                                      this.code = err.code;
                                                      this.message = err.message;
                                                      console.log("error" + this.code);
                                                      console.log("output" + this.message);
                                                     })
    }
    else
    {
      this.validation = "Password must contain special characters";
    }
  }

  constructor(private router:Router,
              private authService:AuthService) { }

  ngOnInit() {
  }

}


