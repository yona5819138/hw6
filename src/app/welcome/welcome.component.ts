import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TodosService } from '../todos.service';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  todos=[];
  todo = '';
  key = this.todos['$key'];
  checkboxFlag: boolean;

  addTodo(){
    this.todosService.addTodo(this.todo);
    this.todo = '';
    
  }

  // checkChange()
  // {
  //   this.todosService.updateDone(this.key, this.todo, this.checkboxFlag);
  // }

  constructor(private authService:AuthService, 
              private db: AngularFireDatabase, 
              private todosService: TodosService) { }

  
              
  ngOnInit() {
    this.authService.user.subscribe(user => {   //פונים לסרביס של יוזר מסוים שהולך לדאטה בייס ומשם ממלא את טודוס במשימות שקיימות לו
      this.db.list('/users/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )
    })

  }

}