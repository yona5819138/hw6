import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toWelcome(){
    this.router.navigate(['/welcome']);
  }
  toSingup(){
    this.router.navigate(['/singup']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/login'])
    }).catch(err => {
      console.log(err)
    });
  }
 
  
  constructor(private router:Router, private authService: AuthService) { }

  ngOnInit() {
  }

}